import 'package:flutter/material.dart';

Widget _checkBox(String title, bool value,ValueChanged<bool?>? onChanged){
  return Container(
    padding: EdgeInsets.only(left: 16, right: 16, bottom: 8),
    child: Column(
      children: [
        Row(
          children: [
            Text('Check 1'),
            Expanded(child: Container()),
            Checkbox(
              value: value, 
              onChanged: onChanged
            )
          ],
        ),
        Divider()
      ],
    )
  );
}

class ComboBoxWidget extends StatefulWidget {
  ComboBoxWidget({Key? key}) : super(key: key);

  @override
  _ComboBoxWidgetState createState() => _ComboBoxWidgetState();
}

class _ComboBoxWidgetState extends State<ComboBoxWidget> {
  bool check1 = false;
  bool check2 = false;
  bool check3 = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text('ComboBox'),),
      body: ListView(
        children: [
          _checkBox('check 1', check1, (value) {
            setState(() {
              check1 = value!;
            });
          }),
          _checkBox('check 2', check2, (value) {
            setState(() {
              check2 = value!;
            });
          }),
          _checkBox('check 3', check3, (value) {
            setState(() {
              check3 = value!;
            });
          }),
          TextButton(
            onPressed: (){
              ScaffoldMessenger.of(context).showSnackBar(
                SnackBar(
                  content: Text('check1: $check1, check2: $check2, check3: $check3')
                )
              );
            },
            child: Text('Save')
          )
        ],
      ),
    );
  }
}
