import 'package:flutter/material.dart';

class DropDownWidget extends StatefulWidget {
  const DropDownWidget({Key? key}) : super(key: key);

  @override
  _DropDownWidgetState createState() => _DropDownWidgetState();
}

class _DropDownWidgetState extends State<DropDownWidget> {
  String vaccine = '-';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('DropDown'),
      ),
      body: Column(
        children: [
          Container(
            padding: EdgeInsets.only(left: 16, right: 16, bottom: 8),
            child: Row(
              children: [
                Text('Karasuno:'),
                Expanded(child: Container()),
                DropdownButton(
                  items: [
                    DropdownMenuItem(
                        child: Text('-'), 
                        value: '-'),
                    DropdownMenuItem(
                        child: Text('Nishinoya Yuu'), 
                        value: 'Nishinoya Yuu'),
                    DropdownMenuItem(
                        child: Text('Kageyama Tobio'),
                        value: 'Kageyama Tobio'),
                    DropdownMenuItem(
                        child: Text('Hinata Shoyo'), 
                        value: 'Hinata Shoyo'),
                    DropdownMenuItem(
                        child: Text('Tsukishima Kei'), 
                        value: 'Tsukishima Kei'),
                    DropdownMenuItem(
                        child: Text('Yamaguji Tadashi'), 
                        value: 'Yamaguji Tadashi'),
                    DropdownMenuItem(
                        child: Text('Tanaka Ryuuno'), 
                        value: 'Tanaka Ryuuno'),
                    DropdownMenuItem(
                        child: Text('Sawamura Daichi'), 
                        value: 'Sawamura Daichi'),
                    DropdownMenuItem(
                        child: Text('Azunane Asahi'), 
                        value: 'Azunane Asahi'),
                    DropdownMenuItem(
                        child: Text('Sugawara Koushi'), 
                        value: 'Sugawara Koushi'),
                  ],
                  value: vaccine,
                  onChanged: (String? newValue) {
                    setState(() {
                      vaccine = newValue!;
                    });
                  },
                )
              ],
            ),
          ),
          Center(
            child: Text(
              vaccine,
              style: TextStyle(fontSize: 24, fontWeight: FontWeight.bold),
            ),
          )
        ],
      ),
    );
  }
}
